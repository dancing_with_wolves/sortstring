//
// Created by dimoha_zadira on 13.09.2019.
//

#ifndef SORTSTRINGS_QUICKSORT_H
#define SORTSTRINGS_QUICKSORT_H

#include <assert.h>

void quickSort( void *arr, int elemSize, int (*compare)(const void*, const void*), int left, int right );


#endif //SORTSTRINGS_QUICKSORT_H
