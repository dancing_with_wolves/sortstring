//
// Created by dimoha_zadira on 13.09.2019.
//

#include <corecrt_malloc.h>
#include "quickSort.h"
typedef int ( *compare_t )( const void*, const void* );
/*!
 * заполняет память to содержимым памяти from
 * @param from указатель, откуда брать память
 * @param to указатель, куда перекладывать значения
 * @param elemSize размер элемента
 */
void fillElemFromPointer( void *from, void *to, int elemSize )
{
    assert( from );
    assert( to );
    for ( int i = 0; i < elemSize; i++ ){
        *(char*)from = *(char*)to;
        from = (char*)from + 1;
        to = (char*)to + 1;
    }
}

/*!
 * Меняет местами содержимое памяти firstElem и secondElem
 * @param firstElem указатель на первый элемент
 * @param secondElem указатель на второй элемент
 * @param elemSize размер элемента
 */
void swap( void *firstElem, void *secondElem, int elemSize )
{
    assert( firstElem );
    assert( secondElem );
    char cur = *(char*)firstElem;
    for (int i = 0; i < elemSize; i++ ){
        cur = *(char*)firstElem;
        *(char*)firstElem= *(char*)secondElem;
        *(char*)secondElem = cur;
        firstElem = (char*)firstElem + 1;
        secondElem = (char*)secondElem + 1;
    }
    /*
    char buffer[elemSize];
    memcpy(buffer, firstElem, elemSize);
    memcpy(firstElem, secondElem, elemSize);
    memcpy(secondElem, buffer, elemSize);*/
}
/*!
 * "раскидывает" все элементы массива относительно стержня, равному среднему элементу массива
 * @param arr сортируемый массив
 * @param elemSize размер элемента
 * @param compare компаратор
 * @param left индекс левого элемента сортируемого подмассива
 * @param right индекс второго элемента сортируемого подмассива
 * @return возвращает новое значение стержня. Справа и слева от него -- два неотсортированных подмассива
 */
int partition( void *arr, int elemSize, compare_t compare, int left, int right )
{
    assert( arr );
    int mid = ( right + left ) / 2;
    void *leftElem = ( char* )arr + ( left * elemSize );
    void *pivot = (char*)arr + ( mid * elemSize );
    swap( leftElem, pivot, elemSize );
    int last = left;
    void *tmp, *tmp2, *root = ( char* )arr + ( left * elemSize );
    for ( int i = left + 1; i <= right; i++ ){
        tmp = (char*)arr + ( i * elemSize );
        if ( compare ( root, tmp ) > 0 ){
            last++;
            tmp2 = (char*) arr + ( last * elemSize );
            swap( tmp2, tmp, elemSize );
        }
    }
    tmp = (char*)arr + ( last * elemSize );
    swap( tmp, root, elemSize );
    return last;
}


/*!
 * Сортирует массив arr в соответствии с компаратором compare
 * @param arr Массив, элементы которого будут отсортированы
 * @param elemSize Размер одного элемента массива
 * @param compare Указатель на функцию, которая сравнивает два элемента и возвращает целое значение >0, если первый элемент больше второго, <0, если первый меньше, =0, если равны
 * @param left индекс самого левого элемента (в общем случае 0)
 * @param right индекс самого правого элемента в общем случае ( n - 1 ), где n -- количество элементов в массиве
 */
void quickSort( void *arr, int elemSize, compare_t compare, int left, int right )
{
    assert( arr );
    if ( left < right ){
        int newIndex = partition ( arr, elemSize, compare, left, right );
        quickSort( arr, elemSize, compare, left, newIndex - 1 );
        quickSort( arr, elemSize, compare, newIndex + 1, right );
    }

}


