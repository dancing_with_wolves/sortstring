//
// Created by dimoha_zadira on 08.09.2019.
//

#ifndef SORTSTRINGS_SORTSTRING_H
#define SORTSTRINGS_SORTSTRING_H

#include "stdlib.h"
#include "stdio.h"

/*!
 * Печатает исходный текст
 * @param text Указатель на первую строчку исходного текста
 * @param lineNumber Количество строк в тексте
 */
void printSource( char *text, int lineNumber, FILE *stream );

/*!
 * Сравнивает строки line1.begin и line2.begin по первым символам (стандартно)
 * @param line1 Ссылка на структуру Line, хранящую первую строку
 * @param line2 Ссылка на структуру Line, хранящую вторую строку
 * @return Возвращает число, >0, если line1 > line2; <0, если line1 < line2; =0, если line1 = line 2
 */
int compareStringsStraight( const void *line1, const void *line2 );

/*!
 * Сравнивает строки line1.begin и line2.begin по последним символам
 * @param line1 Ссылка на структуру Line, хранящую первую строку
 * @param line2 Ссылка на структуру Line, хранящую вторую строку
 * @return Возвращает число, >0, если line1 > line2; <0, если line1 < line2; =0, если line1 = line 2
 */
int compareStringsBack( const void *line1, const void *line2 );

/*!
 * Структура, характеризующая одну строку указателем на её начало и её длиной
 */
struct Line{
    char *begin;
    int length;
};
/*!
 * Функция, печатающая содержимое строк текста
 * @param text Указатель на текст
 * @param lineNumber Количество строк в тексте
 */
void printText ( Line *text, int lineNumber, FILE *stream );
/*!
 * Считывает данные файла fileName побайтово в строку out
 * @param fileName Имя исходного файла
 * @param out Строка, в которую читается файл
 * @param fileSize Размер файла в байтах
 * @return Возвращает код ошибки
 */
int readFile ( char *fileName, char *out, int fileSize );
/*!
 * Возвращает размер файла в байтах
 * @param fileName Имя исходного файла
 * @return Возвращает размер файла в байтах
 */
int countFileSize ( char *fileName );
/*!
 * Возвращает количество строк в буфере
 * @param data Буфер, в который записано содержимое файла
 * @param dataSize Размер data в байтах
 * @return Возвращает количество строк в буфере
 */
int countLineNumber ( const char *data, const int dataSize );
/*!
 *
 * @param out Массив строк, на основании которого будет построен текст
 * @param sausageData Буфер, в который записано содержимое файла
 * @return
 */
int buildText( Line *out, char *sausageData );
/*!
 * Сортирует строки в алфавитном порядке по первым или последним символам в зависимости от параметра mode
 * @param text Массив строк
 * @param mode Тип сортировки: 1 -- прямая, не 1 -- обратная
 * @param lineNumber Количество строк в тексте
 * @return
 */
int sortText( Line *text, const int mode, int lineNumber );


#endif //SORTSTRINGS_SORTSTRING_H
