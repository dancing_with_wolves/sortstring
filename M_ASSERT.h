//
// Created by dimoha_zadira on 13.09.2019.
//

#ifndef SORTSTRINGS_M_ASSERT_H
#define SORTSTRINGS_M_ASSERT_H
#define MY_ASSERT( cond ) {                                                                                   \
    if(!(cond)){                                                                                              \
        printf("MY_ASSERTION FAILED: %s, file %s, line %d, func %s\n", #cond, __FILE__, __LINE__, __func__);  \
        abort();                                                                                              \
    }                                                                                                         \
}

#endif //SORTSTRINGS_M_ASSERT_H
