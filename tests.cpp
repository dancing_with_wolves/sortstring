//
// Created by dimoha_zadira on 13.09.2019.
//

#include <stdio.h>
#include <corecrt_malloc.h>
#include "tests.h"
#include "M_ASSERT.h"
#include "sortString.h"


int test_printText ()
{
    printf("HELLO THERE FROM %s\n-------------------------------------------\n", __func__);
    Line *test = nullptr;

    printText( test, 0, stdout );

    test = ( Line* ) malloc ( 3 * sizeof( Line) );
    test[0].length = 2;
    test[0].begin = "23";
    test[1].length = 4;
    test[1].begin = "fsdr";
    test[2].length = 7;
    test[2].begin = "asdvngh";
    printText( test, 3, stdout );
    return 0;

}

int test_compareStringsStraight()
{
    printf("HELLO THERE FROM %s\n-------------------------------------------\n", __func__);
    Line *test1 = ( Line* ) malloc (sizeof( Line ) );
    Line *test2 = ( Line* ) malloc (sizeof( Line ) );

    test1 -> begin = "123";
    test2 -> begin = "256";
    test2 -> length = 3;
    test2 -> length = 3;
    compareStringsStraight( nullptr, nullptr );
    compareStringsStraight( test1, test2 );

    return 0;
}

int test_compareStringsBack()
{
    printf("HELLO THERE FROM %s\n", __func__);
    Line *test1 = ( Line* ) malloc (sizeof( Line ) );
    Line *test2 = ( Line* ) malloc (sizeof( Line ) );

    test1 -> begin = "123";
    test2 -> begin = "256";
    test1 -> length = 3;
    test2 -> length = 3;
    compareStringsBack( nullptr, nullptr );
    compareStringsBack( test1, test2 );
    printf("-------------------------------------------\n");
    return 0;
}

int test_sortText()
{
    printf("HELLO THERE FROM %s\n", __func__);
    int lineNumber = 3;
    Line *text = ( Line* ) malloc ( lineNumber * sizeof( Line) );

    test_compareStringsStraight();
    test_compareStringsBack();
    printf("-------------------------------------------\n");
    return 0;
}

int test_readFile ()
{
    printf("HELLO THERE FROM %s\n", __func__);
    printf("-------------------------------------------\n");
    return 0;
}

int test_countFileSize ()
{
    printf("HELLO THERE FROM %s\n", __func__);
    FILE *test1 = fopen ( "test1.txt", "w");
    fputs ( "helloWorld!", test1 );
    fclose( test1 );
    printf( "size of test1.txt = %d\n",  countFileSize( "test1.txt") );
    printf("-------------------------------------------\n");
    return 0;
}

int test_countLineNumber ()
{
    printf("HELLO THERE FROM %s\n", __func__);
    char test1[] = "\n\n\n\n\n\n\n\n     \n 123\n /1\n wer\n rt";
    printf( "lineNumber of string %s is %d\n", test1, countLineNumber( test1, sizeof( test1 ) ) );
    printf("-------------------------------------------\n");
    return 0;
}

int test_buildText()
{
    printf("HELLO THERE FROM %s\n", __func__);
    printf("-------------------------------------------\n");
    return 0;
}


