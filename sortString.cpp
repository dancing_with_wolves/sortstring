//
// Created by dimoha_zadira on 08.09.2019.
//
#include <stdio.h>
#include <stdlib.h>
#include "sortString.h"
#include "M_ASSERT.h"
#include "quickSort.h"


void printSource( char *text, int lineNumber, FILE *stream )
{
    for ( int i = 0; i < lineNumber; i++ ){
        while( *text != '\0' ){
            fputc( *text, stream );
            text++;
        }
        fputc( '\n', stream );
        text++;
    }
    fputs( "-------------------------------------------\n", stream );
}

void printText ( Line *text, int lineNumber, FILE *stream )
{
    MY_ASSERT( text );
    int i = 0;
    for ( i = 0; i < lineNumber; i++ ){
        fputs( text[i].begin, stream );
        fputc( '\n', stream );
    }
    printf( "-------------------------------------------\n" );
}



int compareStringsStraight( const void *line1, const void *line2 )
{
    MY_ASSERT( line1 );
    MY_ASSERT( line2 );
    char *str1 = ( ( Line* ) line1 ) -> begin, *str2 = ( ( Line* ) line2 ) -> begin;
    MY_ASSERT( str1 );
    MY_ASSERT( str2 );
    while ( ( *str1 == *str2 ) && ( *str1 != '\0' ) ){
        str1++;
        str2++;
    }
    return *(str1) - *(str2);
}

int compareStringsBack( const void *line1, const void *line2 )
{
    MY_ASSERT( line1 );
    MY_ASSERT( line2 );
    char *str1 = ((Line*)line1) -> begin + ((Line*)line1) -> length - 1;
    char *str2 = ((Line*)line2) -> begin + ((Line*)line2) -> length - 1;
    MY_ASSERT( str1 );
    MY_ASSERT( str2 );
    while ( ( *str1 == *str2 ) && ( *str1 != '\0' ) && ( str1 >= ((Line*)line1) -> begin ) && ( ( str2 >= ((Line*)line2) -> begin ) ) ){
        str1--;
        str2--;
    }
    return *(str1) - *(str2);
}

int sortText( Line *text, const int mode, int lineNumber )
{
    MY_ASSERT( text );
    if ( mode == 1 ){ //энциклопедия
        quickSort( text, sizeof( Line ), compareStringsStraight, 0, lineNumber - 1 );
    } else {//пособие начинающего рэпера
        quickSort( text, sizeof( Line ), compareStringsBack, 0, lineNumber - 1 );
    }
    return 0;
}

int readFile ( char *fileName, char *out, int fileSize )
{
    MY_ASSERT( fileName );
    MY_ASSERT( out );
    FILE *in = fopen ( fileName, "r" );
    MY_ASSERT( in );
    if ( !in ) return -1;
    fread( out, sizeof ( char ), fileSize, in );
    fclose( in );
    return 0;
}

int countFileSize ( char *fileName )
{
    MY_ASSERT ( fileName );
    FILE *in = (FILE*) fopen ( fileName, "r" );
    MY_ASSERT( in );
    if ( !in ) return -1;
    int fileSize = 0;
    fseek( in, 0, SEEK_END );
    fileSize = ftell( in );
    fclose( in );
    return fileSize;
}

int countLineNumber ( const char *data, const int dataSize )
{
    MY_ASSERT( data );
    int lineNumber = 0;
    for ( int i = 0; i < dataSize; i++){
        if( data[i] == '\n' ) {
            //data[i] = '\0';
            lineNumber++;
        }
    }
    return lineNumber;
}

int buildText( Line *out, char *sausageData )
{
    MY_ASSERT( out );
    MY_ASSERT( sausageData );
    int curLen = 0;
    (*out).begin = sausageData;
   while ( *sausageData ){
        if ( *sausageData == '\n' ){
            *sausageData = '\0';
            out++;
            (*out).begin = sausageData + 1;
            (*( out-1 )).length = curLen;
            curLen = -1;
        }
       sausageData++;
       curLen++;
    }
    return 0;
}



