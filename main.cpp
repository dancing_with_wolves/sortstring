#include "sortString.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "tests.h"

int main( int argc, char **argv )
{
    int test = 0;
    char *fileName;

    if ( argc > 1 ) test = atoi ( argv[1] );
    if ( argc > 2 ) fileName = strdup ( argv[2] ); else fileName = strdup( "E:/inputs/input.txt" );

    SetConsoleOutputCP( CP_UTF8 );


    int fileSize = countFileSize ( fileName );
    char *sausageData = ( char* ) calloc ( fileSize + 1, sizeof( char ) );
    if ( test ) {
        test_printText();
        test_countFileSize();
        test_sortText();
        test_readFile();
        test_countLineNumber();
        test_buildText();
    }

    readFile( fileName, sausageData, fileSize );

    int len = strlen( sausageData );
    if( sausageData[ len - 1 ] != '\n') sausageData[ len ] = '\n'; //в конце файла всегда должен быть \n. Может быть, два подряд.

    char *sourceText = sausageData;
    int lineNumber = countLineNumber( sausageData, fileSize + 1 );
    Line* text = ( Line* ) malloc ( lineNumber * sizeof( Line ) );
    buildText( text, sausageData );

    printf("Исходный текст:\n" );
    printSource( sourceText, lineNumber, stdout );

    sortText( text, 1, lineNumber );
    printf( "Отсортировано по первым буквам строк:\n" );
    printText( text, lineNumber, stdout );

    sortText( text, 2, lineNumber );
    printf( "Отсортировано по последним буквам строк:\n" );
    printText( text, lineNumber, stdout );

    return 0;
}
