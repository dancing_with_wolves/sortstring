//
// Created by dimoha_zadira on 13.09.2019.
//

#ifndef SORTSTRINGS_TESTS_H
#define SORTSTRINGS_TESTS_H

#include <corecrt_wstdio.h>
#include "sortString.h"

int test_printText ();

int test_compareStringsStraight();

int test_compareStringsBack();

int test_sortText();

int test_readFile ();

int test_countFileSize ();

int test_countLineNumber ();

int test_buildText();


#endif //SORTSTRINGS_TESTS_H
